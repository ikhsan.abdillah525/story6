from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('add_kegiatan', views.add_kegiatan, name='add_kegiatan'),
    path('add_peserta', views.add_peserta, name='add_peserta'),
]
