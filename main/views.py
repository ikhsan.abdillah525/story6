from django.shortcuts import render, redirect
from .models import Activity, Peserta
from .forms import FormKegiatan, FormPeserta

def home(request):
    allActivity = Activity.objects.all()

    return render(request, 'main/home.html', {'allActivity' : allActivity})

def add_kegiatan(request):
    if request.method == "POST":
        nama = request.POST['nama']
        kegiatan_baru = Activity(nama = nama)
        kegiatan_baru.save()

        return redirect('../')

    else:
        form = FormKegiatan()
    
    return render(request, 'main/add_kegiatan.html', {'form' : form})

def add_peserta(request, id):
    if request.method == "POST":
        nama = request.POST['nama']
        kegiatan = request.POST['kegiatan']
        peserta = Peserta(nama, kegiatan)
        peserta.save()

        return redirect('../')

    else:
        form = FormPeserta()
    
    return render(request, 'main/add_peserta.html', {'form' : form})