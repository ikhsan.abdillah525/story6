from django.db import models

# Create your models here.
class Activity(models.Model):
    nama = models.TextField(max_length=30)

    def __str__(self):
        return self.nama

class Peserta(models.Model):
    nama = models.CharField(max_length=30)
    kegiatan = models.ManyToManyField(Activity)

    def __str__(self):
        return self.nama