from django import forms

class FormKegiatan(forms.Form):
    nama = forms.CharField(label="Nama kegiatan")

class FormPeserta(forms.Form):
    nama = forms.CharField(label="Nama anda")
    kegiatan = forms.CharField(label="Nama kegiatan")